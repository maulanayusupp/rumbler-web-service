<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('userid')->unsigned();
			$table->foreign('userid')->references('id')->on('users');
			$table->string('title',32);
			$table->string('image',128);
			$table->text('content');
			$table->integer('is_video');
			$table->integer('is_quote');
			$table->integer('is_link');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
