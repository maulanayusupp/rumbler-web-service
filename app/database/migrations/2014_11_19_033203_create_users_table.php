<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name',128);
			$table->string('username',32);
			$table->string('email',32)->unique();
			$table->dateTime('birthday');
			$table->string('password',100);
			$table->text('description');
			$table->string('blog_title',32);
			$table->string('picture_path',225);
			$table->dateTime('last_login');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
